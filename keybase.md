### Keybase proof

I hereby claim:

  * I am ams0 on github.
  * I am avozza (https://keybase.io/avozza) on keybase.
  * I have a public key whose fingerprint is 7F41 08C6 5716 2C8B EF14  31F4 124A 6BE0 81F4 213A

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "eldest_kid": "010170c45258f64964ca0022480dbe92eca031aa95b95650d2a9017dd84cb13319a20a",
            "fingerprint": "7f4108c657162c8bef1431f4124a6be081f4213a",
            "host": "keybase.io",
            "key_id": "124a6be081f4213a",
            "kid": "010170c45258f64964ca0022480dbe92eca031aa95b95650d2a9017dd84cb13319a20a",
            "uid": "c09274a98bea8e81c7ee96a78fe25019",
            "username": "avozza"
        },
        "service": {
            "name": "github",
            "username": "ams0"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1458040573,
    "expire_in": 157680000,
    "prev": "25eb57cedd60a5da08078afd20c40dade41785f93540ad0654d4909853541969",
    "seqno": 2,
    "tag": "signature"
}
```

with the key [7F41 08C6 5716 2C8B EF14  31F4 124A 6BE0 81F4 213A](https://keybase.io/avozza), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Comment: GPGTools - http://gpgtools.org

owGtkltIVEEYx3drrVwyuzxk2oWOQWRLzZw9cy5rYUFSmJhZmoS0zjkzZzvYXjxn
d/OS9dJViiSCAqXEULQMI4KgC4kIReVDdBESu2lIYgWVaFTWHKmX6LF5mfm++f3/
/Odj6pKmOtzO2kIyGE2tOud88E51FA19dFZzaphUcr5qroxObnQPoVbUX2YQzscB
CKAENAHxSNZFQREFDQPA84IMiEoVnrLSCzFWkKogEQHCY4UpCJEFTYVeL1QwDzDn
4XQjFKBmxDRCUWYr6QIEsiYiCYq8JqtUh4IXsiYvYFGlQGZnHnpt4e6wZStYOBVb
dJURZj1W+Cfj/YP/z7ljk3YaUHhJwApLimUqQ02iVBGxJOuURwAqNmhRM4SDlNE4
Hq6qwlyNh2O9uKFRe6y/7wJGdHdM/YsPWsCmo5URu9xLVf9voV81QoRNjvFxalpG
OMT5ICO1qGEroYBkIAAkeT0crYgYJvUbNoEkUQZsebiISePMkkdURZJGCREBRgQD
GUhyDtYJz2YECCZUgJKMdMWLBIAJEJFABAUoMmINqIj2Ay1aHgpzPp7lxAHmaRmB
EI7GTMrVuI85F7ocTrdjWsIU+0853Imz/3y055umTywfX3tAzC/MbDlZn3StNKF+
zbzL0mBTVwxnbfp5sG/R8VsXxsbbk9et/tC7IPHbcNbA7Q0pL3Lvz0hb2XfwZ+n2
utE57r33CvSXJJL/7bUzoyuvcnHKo115Hw9v3NY3wn0tK2kbr+1o/VxbvGpnqthw
NcEV2td86Ej9XDWtsmHsaPJw94bR4rNHm9Jz254cysq+3vPGPH0ivTex7tXNi29d
zbMaXRVPv5efGe7fkd0wgdvmd+6f+fjCFf3upc8jzTfcOc5P65e1rthDspf4Kj4l
bd3S8X6zUvKwsYh0LnUHB+CJpyr+2n+koHPB+S/tmdz9GeHhZadagmMD+wM9d36M
PhuSuzN+AQ==
=d+h6
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/avozza

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id avozza
```
